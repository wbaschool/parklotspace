﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using ParkLotSpace.Models;

namespace ParkLotSpace.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Address> address = new List<Address>();
            address.Add( new Address() { Id = 1, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 2, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 3, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 4, Name = "Bruuns Galleri", Street = "M.P. Bruuns Gade", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 5, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 6, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 7, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 8, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 9, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 10, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 11, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 12, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });
            address.Add( new Address() { Id = 13, Name = "", Street = "", ZipCode = 8000, City = "Aarhus", Latitude = 0.0m, Longitude = 0.0m });

            var client = new WebClient();
            var json = client.DownloadString("http://www.odaa.dk/api/action/datastore_search?resource_id=2a82a145-0195-4081-a13c-b0e587e9b89c");
            parkHouse parkhouse = new parkHouse();

            parkhouse = JsonConvert.DeserializeObject<parkHouse>(json);

            return View(parkhouse.result.records);
        }
    }
}