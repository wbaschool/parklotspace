﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkLotSpace.Models
{
    public class Links
    {
        public string start { get; set; }
        public string next { get; set; }
    }
}
