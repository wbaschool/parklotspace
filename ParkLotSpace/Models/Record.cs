﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkLotSpace.Models
{
    public class Record
    {
        public string date { get; set; }
        public int vehicleCount { get; set; }
        public int _id { get; set; }
        public int totalSpaces { get; set; }
        public string garageCode { get; set; }
        public int freeSpace => (totalSpaces - vehicleCount);
        public string Name { get; set; }
        public string Street { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
