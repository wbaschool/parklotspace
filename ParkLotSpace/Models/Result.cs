﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkLotSpace.Models
{
    public class Result
    {
        public string resource_id { get; set; }
        public List<Field> fields { get; set; } = new List<Field>();
        public List<Record> records { get; set; } = new List<Record>();
        public Links _links { get; set; } = new Links();
        public int total { get; set; }
    }
}
